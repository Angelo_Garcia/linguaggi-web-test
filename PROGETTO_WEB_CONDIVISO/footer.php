    <div class="bottombar">
        <small>
            &emsp;Questo sito web è stato realizzato da Diego Petrucci e Angelo Garcia, studenti di Ingegneria Informatica dell'università Sapienza.<br>
            &emsp;Contatti email:&emsp;petrucci.1783255@studenti.uniroma1.it&emsp;-&emsp;garcia.1812424@studenti.uniroma1.it <br>
            &emsp;Repository GIT:&emsp;<a class="myAnchor" href="https://gitlab.com/Angelo_Garcia/linguaggi-web-test" target="_blank">https://gitlab.com/Angelo_Garcia/linguaggi-web-test</a>
        </small>
    </div>

</body>
</html>